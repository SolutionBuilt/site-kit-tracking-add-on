<?php
/**
 * Plugin Name: Site Kit Tracking Add-On
 * Description: Site Kit by Google add-on to exclude users with administrator or editor role from Tag Manager tracking.
 * Version: 1.3
 * Update URI: https://bitbucket.org/SolutionBuilt/wordpress-plugin-updates/raw/master/info.json
 * Author: SolutionBuilt
 * Author URI: https://www.solutionbuilt.com
 * License: GPL-2.0+
 */


/* ---------------------------------------------------------------------------
 * Remove Google Analytics for administrator users
 *
 * This is now configured through Site Kit plugin's "Exclude from Analytics" setting.
 * --------------------------------------------------------------------------- */
// add_filter( 'googlesitekit_analytics_tag_blocked', function( $value ) {
// add_filter( 'googlesitekit_analytics4_tag_blocked', function( $value ) {


/* ---------------------------------------------------------------------------
 * Remove Google Tag Manager for administrator users
 * --------------------------------------------------------------------------- */

add_filter(
	'googlesitekit_tagmanager_tag_blocked',
	function( $value ) {
		return current_user_can( 'edit_posts' ) && ! isset( $_GET['gtm_debug'] ) && ! isset( $_COOKIE['gtm_debug'] );
	}
);

// Enable Google Tag Manager while debugging
add_action(
	'init',
	function() {
		if ( isset( $_GET['gtm_debug'] ) ) {
			setcookie( 'gtm_debug', $_GET['gtm_debug'], time() + 1800 );
		}
	}
);


/* ---------------------------------------------------------------------------
 * Require Site Kit by Google plugin to be active
 * --------------------------------------------------------------------------- */

// Automatically activate Site Kit plugin if needed
register_activation_hook(
	__FILE__,
	function() {
		$plugin = 'google-site-kit/google-site-kit.php';

		if ( file_exists( WP_PLUGIN_DIR . '/' . $plugin ) && ! is_plugin_active( $plugin ) ) {
			activate_plugin( $plugin );
		}
	}
);

// Prevent plugin activation if Site Kit is not installed or is blocked by WP Local Toolbox
add_action(
	'admin_init',
	function() {
		// Note: Use register_activation_hook to activate Site Kit plugin if installed.
		//	   Attempting to activate here will work, but won't show the plugin as active until the next page load.

		if ( current_user_can( 'activate_plugins' ) && ! is_plugin_active( 'google-site-kit/google-site-kit.php' ) ) {
			deactivate_plugins( plugin_basename( __FILE__ ) );

			add_action(
				'admin_notices',
				function() {
					unset( $_GET['activate'] );

					echo '<div class="notice notice-error is-dismissible"><p>Plugin cannot be activated. Please install and activate Site Kit by Google plugin first.</p></div>';
				}
			);
		}
	}
);

// Automatically deactivate if Site Kit plugin is deactivated
add_action(
	'deactivated_plugin',
	function( $plugin, $network_activation ) {
		if ( 'google-site-kit/google-site-kit.php' === $plugin ) {
			add_action(
				'update_option_active_plugins',
				function() {
					deactivate_plugins( plugin_basename( __FILE__ ) );
				}
			);
		}
	},
	10,
	2
);


/* ---------------------------------------------------------------------------
 * Check for update
 * --------------------------------------------------------------------------- */

add_filter(
	'update_plugins_bitbucket.org',
	function( $update, $plugin_data, $plugin_file, $locales ) {
		static $raw_response = false; // Prevent duplicate requests

		if (
			plugin_basename( __FILE__ ) !== $plugin_file ||
			empty( $plugin_data['UpdateURI'] ) ||
			! empty( $update )
		) {
			return $update;
		}

		if ( false === $raw_response ) {
			$raw_response = wp_remote_get(
				$plugin_data['UpdateURI'],
				array(
					'timeout' => 10,
					'headers' => array(
						'Accept' => 'application/json',
					),
				)
			);
		}

		$response = json_decode( wp_remote_retrieve_body( $raw_response ), true );

		// error_log( print_r( $response, false ), 3, __DIR__ .'/log.txt' );

		if ( ! empty( $response ) && ! empty( $response[ $plugin_file ] ) ) {
			return $response[ $plugin_file ];
		}

		return $update;
	},
	10,
	4
);
